﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour
{
    [SerializeField] private List<SpawnerItem> spawnElements;
    private Renderer renderer;
    private Vector2 spawnPoint;

    // Start is called before the first frame update
    void Start()
    {        
        renderer = GetComponent<Renderer>();
        foreach(SpawnerItem item in spawnElements)
        {
            StartCoroutine("SpawnElement", item);
        }
    }

    IEnumerator SpawnElement(SpawnerItem item)
    {
        while (gameObject.activeSelf) 
        {
            GenerateSpawnPoint();
            Instantiate(item.SpawnElement, spawnPoint, Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(item.LowTimeSpawn, item.UpTimeSpawn));
        }
    }

    private void GenerateSpawnPoint()
    {
        float x1 = transform.position.x - renderer.bounds.size.x / 2;
        float x2 = transform.position.x + renderer.bounds.size.x / 2;
        spawnPoint = new Vector2(Random.Range(x1, x2), transform.position.y);
    }
}
