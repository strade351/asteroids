﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayUI : MonoBehaviour
{
    [SerializeField] private Text scoresText;
    [SerializeField] private Text livesText;

    void Start() 
    {
        scoresText.text = "Scores: " + GameInfo.Scores.ToString();
        livesText.text = "Lives: " + GameInfo.PlayerLives.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        scoresText.text = "Scores: " + GameInfo.Scores.ToString();
        livesText.text = "Lives: " + GameInfo.PlayerLives.ToString();
    }
   
    public void DoExitGame() 
    {
        Application.Quit();
    }
}
