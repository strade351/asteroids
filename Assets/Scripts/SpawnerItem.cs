﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerItem : MonoBehaviour
{
    public GameObject SpawnElement => spawnElement;
    public float UpTimeSpawn => upTimeSpawn;
    public float LowTimeSpawn => lowTimeSpawn;

    [SerializeField] private GameObject spawnElement;
    [SerializeField] private float upTimeSpawn;
    [SerializeField] private float lowTimeSpawn;

    void Start()
    {
        spawnElement = gameObject;
    }
}
