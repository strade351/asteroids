﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusScript : MonoBehaviour
{
    public int Lives => lives;
    public int Scores => scores;

    [SerializeField] private int speed;
    [SerializeField] private int lives;
    [SerializeField] private int scores;
        
    private Rigidbody2D rigidBody;
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(rigidBody.velocity.x, -speed);
    }

    void OnBecameInvisible() 
    {
        Destroy(gameObject);
    }
}
