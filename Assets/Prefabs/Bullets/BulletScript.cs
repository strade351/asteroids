﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int Damage => damage;

    [SerializeField] private int speed;
    [SerializeField] private int damage;
    [SerializeField] private BulletSides bulletType;

    private Rigidbody2D rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        if (bulletType.Equals(BulletSides.PlayerBullet))
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, speed);
        }
        else
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, -speed);
        }
    }

    void OnBecameInvisible() 
    {
        Destroy(gameObject);
    }

    enum BulletSides
    {
        EnemyBullet,
        PlayerBullet
    }
}
