﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int Reward => reward;

    [SerializeField] private float speed;
    [SerializeField] private float shootDelay;
    [SerializeField] private GameObject bullet;
    [SerializeField] private int reward;
    [SerializeField] private int lives;
    [SerializeField] private GunVariants gun;

    private Rigidbody2D rigidBody;
    private Renderer renderer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(rigidBody.velocity.x, -speed);
        InvokeRepeating("Shoot", 0, shootDelay);
    }
    
    void Shoot()
    {
        if(gun == GunVariants.OneGun)
        {
            Instantiate(bullet, transform.localPosition, new Quaternion());
        }
        if(gun == GunVariants.TwoGun)
        {
            float gunOnePosition = transform.localPosition.x - 0.5f;
            float gunTwoPosition = transform.localPosition.x + 0.5f;
            Instantiate(bullet, new Vector2(gunOnePosition, transform.localPosition.y), new Quaternion());
            Instantiate(bullet, new Vector2(gunTwoPosition, transform.localPosition.y), new Quaternion());
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) 
    {
        string name = collider.tag;
        if (name.Equals("PlayerBullet")) 
        {
            int bulletDamage = collider.GetComponent<BulletScript>().Damage;
            TakeDamage(bulletDamage);
            Destroy(collider.gameObject);
        }
    }

    private void TakeDamage(int damage)
    {
        lives -= damage;
        if(lives <= 0)
        {
            Death();
        }
    }

    private void Death()
    {
        GameInfo.ScoresUp(reward);
        Destroy(gameObject);
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    enum GunVariants 
    {
        OneGun,
        TwoGun
    }
}