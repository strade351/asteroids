﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : MonoBehaviour
{
    [SerializeField] private GameObject Player;
    [SerializeField] private Canvas ui;
    [SerializeField] private int startLives;

    private static int playerLives;
    private static int scores;

    public static int PlayerLives => playerLives;
    public static int Scores => scores;

    void Start() 
    {
        playerLives = startLives;
    }

    public static void ScoresUp(int count) 
    {
        scores += count;
    }

    public static void LivesUp(int count) 
    {
        playerLives += count;
    }

    public static void LivesDown(int count)
    {
        playerLives -= count;
        if (playerLives == 0)
        {
            GameOver();
        }
    }

    static void GameOver()
    {
        Debug.Log("Game Over");
        Application.Quit();
    }
}
