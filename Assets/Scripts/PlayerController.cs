﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    private Rigidbody2D rigidBody;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();        
    }

    private void OnTriggerEnter2D(Collider2D collider) 
    {
        string name = collider.tag;
        if (name.Equals("EnemyBullet")) 
        {
            int damage = collider.GetComponent<BulletScript>().Damage;
            GameInfo.LivesDown(damage);
            Destroy(collider.gameObject);
        }
        if (name.Equals("Enemy"))
        {
            GameInfo.LivesDown(1);
            Destroy(collider.gameObject);
        }
        if (name.Equals("Asteroid"))
        {
            int damage = collider.GetComponent<AsteroidScript>().Damage;
            GameInfo.LivesDown(damage);
            Destroy(collider.gameObject);
        }
        if (name.Equals("Bonus"))
        {
            int lives = collider.GetComponent<BonusScript>().Lives;
            int scores = collider.GetComponent<BonusScript>().Scores;
            GameInfo.LivesUp(lives);
            GameInfo.ScoresUp(scores);
            Destroy(collider.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //movement
        if (Input.GetKey("right")) 
        {
            rigidBody.velocity = new Vector2(10, rigidBody.velocity.y);
        }
        else if (Input.GetKey("left"))
        {
            rigidBody.velocity = new Vector2(-10, rigidBody.velocity.y);
        }
        else
        {
            rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
        }

        //shooting
        if (Input.GetKeyDown("space"))
        {
            Instantiate(bullet, transform.position, Quaternion.identity);
        }
    }
}
