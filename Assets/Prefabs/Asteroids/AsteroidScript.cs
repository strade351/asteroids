﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidScript : MonoBehaviour
{
    public int Damage => damage;
    public int Reward => reward;

    [SerializeField] private int speed;
    [SerializeField] private bool isRotate;
    [SerializeField] private int rotationBorder;
    [SerializeField] private int reward;
    [SerializeField] private int damage;

    private Rigidbody2D rigidBody;
    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        rigidBody.velocity = new Vector2(rigidBody.velocity.x, -speed);
        
        //rotation
        if (isRotate)
        {
            rigidBody.angularVelocity = Random.Range(-rotationBorder, rotationBorder);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) 
    {
        string name = collider.tag;
        if (name.Equals("PlayerBullet")) 
        {
            GameInfo.ScoresUp(reward);
            Destroy(gameObject);
            Destroy(collider.gameObject);
        }
    }
    
    private void OnBecameInvisible() 
    {
        Destroy(gameObject);
    }
}
